Array.prototype.newForEach = function(a){
    for(let i = 0; i < this.length; i ++){
        a(this[i]);
    }
}
Array.prototype.newMap = function(func){
    let temp = [];
    for(let i = 0; i < this.length; i ++){
        temp.push(func(this[i]));
    }
    return temp;
}
Array.prototype.newSome = function(func){
    for(let i = 0; i < this.length; i ++){
        if(func(this[i]) === true)
            return true;
    }
    return false;
}
Array.prototype.newFind = function(func){
    for(let i = 0; i < this.length; i ++){
        if(func(this[i]) === true)
            return this[i];
    }
}
Array.prototype.newFindIndex = function(func){
    for(let i = 0; i < this.length; i ++){
        if(func(this[i]) === true)
            return i;
    }
    return -1;
}
Array.prototype.newEvery = function(func){
    for(let i = 0; i < this.length; i ++){
        if(func(this[i]) !== true)
            return false;
    }
    return true;
}
Array.prototype.newFilter = function(func){
    let temp = [];
    for(let i = 0; i < this.length; i ++){
        if(func(this[i]) === true)
            temp.push(this[i]);
    }
    return temp;
}
Array.prototype.newConcat = function(arr2){
    let temp = [];
    for(let i = 0; i < this.length; i ++)
        temp.push(this[i]);
    for(let i = 0; i < arr2.length; i ++)
        temp.push(arr2[i]);
    return temp;
}
Array.prototype.newIncludes = function(value){
    for(let i = 0; i < this.length; i ++)
        if(this[i] === value)
            return true;
    return false;
}
Array.prototype.newIndexOf = function(value){
    for(let i = 0; i < this.length; i ++)
        if(this[i] === value)
            return i;
    return -1;
}
Array.prototype.newJoin = function(str){
    let retString = '';
    for(let i = 0; i < this.length; i ++){
        retString += this[i];
        if(i != this.length - 1)
            retString += str;
    }
    return retString;
}
Array.prototype.newSlice = function(begin, end){
    let temp = [];
    for(let i = begin; i < end && i < this.length; i ++){
        temp.push(this[i]);
    }
    return temp;
}
Array.prototype.newOf = function(){
    let temp = [];
    for(let i = 0; i < arguments.length; i ++){
        temp.push(arguments[i]);
    }
    return temp;
}